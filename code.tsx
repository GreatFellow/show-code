// Это пример кастомного хука, который я давно написал для одного из проектов,
// для которого я руковожу разработкой интерфейса. Само приложение построено на
// архитектуре микрофронтендов с помощью плагина Module Federation для вебпака и
// выстроено следующим образом: у нас есть мастер приложение (core-ui) и есть набор
// модулей. Каждый из модулей и кор юай упакованы в свой докер контейнер и все они хостятся
// в кубернетесе каждый на своём урле. Доступ ко всему приложению осуществляется
// только через урл кора. А кор, в свою очередь, подтягивает необходимые удалённые модули
// с их урлов, и собирает это всё в единое приложение. Сам кор отвечает ещё и за отрисовку
// навигационного бокового меню и некоторые другие штуки, которые для этого примера не важны.
// Мы с архитектором хотели, чтобы при открытии приложения кор юай знал настолько
// мало информаци про удалённые модули, которые ему надо подтянуть, насколько это возможно.
// У кора есть файл remotesConfig.js, содержащий инфу о модулях в виде массива объектов. Там прописаны их урлы
// (точнее, урлы подставляются из переменных окружения, куда их динамически подсовывает
// Helm Chart при запуске докер образа), наличие у модуля т.н. подмодулей и путь, по которому их можно получить
// (у пункта нав. меню, соответствующего какому-либо модулю, могут быть вложенные подпункты, ведущие на различные
// части функционала данного модуля) и разная нужная для module federation инфа.
// Так вот, этот кастомный хук принимает на вход этот конфиг, собирает у модулей недостающую
// инфу о них, например, список подмодулей, встраивает скрипт файлы модулей в html, чтобы с ними
// можно было работать, формирует и возвращает уже новый конфиг (тоже массив объектов), из которого
// кор юай уже может выстроить и отрисовать правильное навигационное меню и способен обеспечить
// lazy подгрузку элементов удалённых микрофронтов тогда, когда юзер нажимает на соответствующую
// кнопку этого нав. меню



import { useEffect, useState } from "react";
import { loadComponent } from "../utils/loadComponent";
import { scriptExisitngChecker } from "../utils/scriptExistingChecker";
import {
  IIncomingRemotesConfig,
  IIncomingRemoteModuleConfig,
  TRemotesDynamicConfig,
  IOutgoingRemotesConfig,
  IOutgoingRemoteModuleConfig,
  ISubModuleConfig,
} from "../types";

// кэш урлов загруженных .js файлов удалённых модулей
const remoteEntriesUrlCache: Set<string> = new Set();

// кэш урлов загруженных .js конфиг файлов, содержащих в себе объекты с указанием значений переменных окружений удалённых модулей.
// Для полной автоматизации CI/CD и процесса деплоя на разные стенды (дев, тест, прод) использовались переменные окружения,
// значения которых менялись с помощью Helm Chart на этапе сборки докер образа. Кор юай должен был знать значения переменных удалённых
// модулей, чтобы модули могли использовать их в рантайме. Для этого кор подтягивал конфиг файлы.
const remoteEnvConfigsUrlCache: Set<string> = new Set();
export const useRemotesDynamicConfig = (
  remotesConfig: IIncomingRemotesConfig
): TRemotesDynamicConfig => {
  // стейт итогового конфига, в качестве инишал значения сюда попадает массив объектов из изначального конфига в корес описанием модулей.
  // Если в конфиге не указано имя модуля, то его не должно быть в интерфейсе, поэтому фильтруем
  // В результате работы этого хука объекты массива remotes получат некоторые изменения
  const [outGoingRemotesConfig, setOutgoingRemotesConfig] = useState<IOutgoingRemotesConfig>({
    remotes: remotesConfig.remotes.filter(remote => remote.name)
  });

  // Из этого массива потом размапим роутер таким образом, что будет возможным лэйзи лоадинг удалённых модулей
  const [remoteModulesConfigArr, setRemoteModulesConfigArr] = useState<IOutgoingRemoteModuleConfig[]>([]);

  // функция загрузки подмодулей удалённых микрофронтов
  const loadRemoteSubmodules = async (
    remote: IIncomingRemoteModuleConfig,
    setOutgoingRemotesConfig: React.Dispatch<
      React.SetStateAction<IOutgoingRemotesConfig | null>
    >
  ) => {
    const outGoingConfig: any = { ...outGoingRemotesConfig };
    if (remote.subModulesPath) {
      // loadComponent - функция из доки Module Federation. Нужна для динамической загрузки удалённых модулей
      const remoteSubModules = loadComponent(
        remote.remoteAppName,
        remote.subModulesPath
      );
      try {
        const remotes = await remoteSubModules();
        const defineRemoteIndex = outGoingConfig.remotes.findIndex(
          (outGoingRemote: IOutgoingRemoteModuleConfig) =>
            outGoingRemote.index === remote.index
        );

        if (defineRemoteIndex >= 0) {
          outGoingConfig.remotes[defineRemoteIndex] = {
            ...remote,
            subModules: [...remotes.default]
          };
          setOutgoingRemotesConfig({ ...outGoingConfig });
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  // функция загрузки конфигов удалённых модулей с переменными окружения. После загрузки скрипт файл сеттится в хтмл.
  const loadAndSetRemoteModuleEnvVars = async (
    remoteModuleEnvConfigURL: string
  ) => {
    const element = document.createElement("script");

    element.src = remoteModuleEnvConfigURL;
    element.type = "text/javascript";
    element.async = true;

    element.onload = () => {
      remoteEnvConfigsUrlCache.add(remoteModuleEnvConfigURL);
    };

    document.head.appendChild(element);
  };

  // функция загрузки .js файлов удалённых модулей. После загрузки скрипт файл сеттится в хтмл.
  const appendScriptFile = async (
    url: string,
    loadRemotesEntitiesFunc: (...args: any) => void,
    ...args: any
  ) => {
    const element = document.createElement("script");

    // таймстэмп для браузерного cache busting
    element.src = `${url}?t=${new Date().getTime()}`;
    element.type = "text/javascript";
    element.async = true;

    element.onload = () => {
      remoteEntriesUrlCache.add(url);
      loadRemotesEntitiesFunc(...args);
    };

    // проверяем прикреплён ли уже скрипт с таким урлом в хтмл
    scriptExisitngChecker(url) && document.head.appendChild(element);
  };

  useEffect(() => {
    (async () => {
      for (const remote of outGoingRemotesConfig.remotes) {
        const remoteEntryFileUrl = `${remote.remoteUrl}/${remote.remoteFilename}`;
        // загружаем сабмодули
        await appendScriptFile(
          remoteEntryFileUrl,
          loadRemoteSubmodules,
          remote,
          setOutgoingRemotesConfig
        );
      }
    })();
  }, []);

  const isSubModulesExist = (submodules: IOutgoingRemoteModuleConfig['subModules']) =>
    submodules && submodules.length;

  // в общем конфиг файле с описанием удалённых модулей изначально указан только путь до сабмодулей.
  // Эта функция позволяет зарегистрировать сабмодули уже как компоненты, с которыми сможет работать кор.
  // Она на всякий случай рекурсивная, если вдруг когда-то у сабмодулей появятся свои сабмодули.
  const registerSubModules = (
    subModules: ISubModuleConfig[],
    parentPath: string,
    remoteUrl: string,
    remoteAppName: string,
    remoteModules: IOutgoingRemoteModuleConfig[],
    needToLoadScript: boolean
  ) => {
    for (const subModule of subModules) {
      const { subModules } = subModule;
      let path = `${parentPath}/${subModule.path}`;

      if (isSubModulesExist(subModules)) {
        registerSubModules(
          subModules!,
          path,
          remoteUrl,
          remoteAppName,
          remoteModules,
          needToLoadScript
        );
        continue;
      }

      const newSubModule = {
        ...subModule,
        path,
        remoteUrl,
        remoteAppName,
      };

      remoteModules.push({ ...newSubModule });
    }
  }

  // юзэффект, завершающий формирование нужного нам конфига и подготавливающий массив для роутера
  useEffect(() => {
    if (outGoingRemotesConfig?.remotes && outGoingRemotesConfig.remotes.length > 0) {
      (async () => {
        const remoteModules: IOutgoingRemoteModuleConfig[] = [];
        for (const remote of outGoingRemotesConfig.remotes) {
          const { remoteUrl, subModules, path, remoteAppName, subModulesPath } = remote;
          const remoteEntryFileUrl = `${remoteUrl}/${remote.remoteFilename}`;

          const needToLoadRemoteEntryScript =
            !remoteEntriesUrlCache.has(remoteEntryFileUrl);

          if (isSubModulesExist(subModules)) {
            registerSubModules(
              subModules!,
              path,
              remoteEntryFileUrl,
              remoteAppName,
              remoteModules,
              needToLoadRemoteEntryScript,
            );
          }
          if (!isSubModulesExist(subModules) && !subModulesPath) {
            remoteModules.push({ ...remote, remoteUrl: remoteEntryFileUrl });
          }
        };
        setRemoteModulesConfigArr(remoteModules);
      })();
    }
  }, [outGoingRemotesConfig]);

  // загружаем конфиги с переменными окружения
  useEffect(() => {
    if (outGoingRemotesConfig?.remotes && outGoingRemotesConfig.remotes.length) {
      (async () => {
        for (const remote of outGoingRemotesConfig.remotes) {
          const remoteModuleEnvConfigURL = `${remote.remoteUrl}/config.js`;
          const needToLoadRemoteEnvConfigScript =
            !remoteEnvConfigsUrlCache.has(remoteModuleEnvConfigURL) &&
            !scriptExisitngChecker(remoteModuleEnvConfigURL);

          if (needToLoadRemoteEnvConfigScript)
            await loadAndSetRemoteModuleEnvVars(remoteModuleEnvConfigURL);
        }
      })();
    }
  }, [outGoingRemotesConfig]);

  return {
    remoteModulesConfigArr,
    remotesConfig: outGoingRemotesConfig,
  };
};

